# Markdown AI Webapp

Webapp that supports a Markdown Editor and "Text to Image Generation" from the text in the markdown.

Author: Ciaala (Francesco Fiduccia)

## Features/Goals:

- Support auto discovery of meaningful description (text in markdown) to transform in an image 
- For each description it generates Several Images and support the user in picking the preferred image.
- Present a preview of the Markdown
- Images after generation are inserted in the markdown with Path corresponding to the URL of the Web Resource

## Modules 


### Server

- LLM and Text To Image are API services delivered by the 'server' module
- Server is a python and FastAPI API Services that also will provide the generated resources for the frontend

### Client 

- Client is a React App in Typescript, its source code is in 'app'. 
- The framework selected is Expo as it a promising framework to generate Native UI

