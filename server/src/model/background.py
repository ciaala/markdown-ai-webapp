from typing import List

from PIL.Image import Image
from transformers import pipeline


class BackgroundRemoval:
    def __init__(self, _pipeline: pipeline) -> None:
        self._pipeline = _pipeline

    def remove(self, images: List[Image]) -> List[Image]:
        # mask = self._pipeline(image, return_mask=True)
        return self._pipeline(images)


class BackgroundRemovalFactory:
    def background_remover(self) -> BackgroundRemoval:
        _pipeline = pipeline("image-segmentation",

                             model="briaai/RMBG-1.4",
                             trust_remote_code=True)
        return BackgroundRemoval(_pipeline)
