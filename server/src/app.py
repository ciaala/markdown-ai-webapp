import os
from dataclasses import dataclass
from typing import List

from src.image.image_generation import ImageGenerator
from src.model.background import BackgroundRemovalFactory
from src.model.text_to_image import TextToImage, TextToImageFactory


@dataclass
class ImageDescription:
    prompt: str
    key: str


def generate_descriptions() -> List[ImageDescription]:
    pre = "minimalist, realistic, 3 colors and with a black contour, like a logo about "
    descriptions = [
        ImageDescription(pre + "white chocolate bar", key="white_chocolate_bar"),
        ImageDescription(pre + "dark chocolate bar", key="dark_chocolate_bar"),
        ImageDescription(pre + "milk chocolate bar", key="milk_chocolate_bar"),

        ImageDescription(pre + "potato pringles", key="potato_pringles"),
        ImageDescription(pre + "potato slices roasted", key="potato_oven_roasted"),
        ImageDescription(pre + "potato chips package", key="potato_chips"),
        ImageDescription(pre + "potato wave chips ", key="potato_wave_chips"),

        ImageDescription(pre + "corn kernels", key="corn_kernels"),
        ImageDescription(pre + "pop corn", key="pop_corn"),
        ImageDescription(pre + "black corn", key="black_corn"),

        ImageDescription(pre + "pizza slice with corn kernels", key="pizza_with_corn"),
        ImageDescription(pre + "pizza slice and a kebap", key="pizza_with_kebap"),
        ImageDescription(pre + "pizza margherita", key="pizza_margherita"),

        ImageDescription(pre + "catalan tomato with bread", key="pan_tomaquet"),
        ImageDescription(pre + "bread with olives", key="bread_with_olives"),
        ImageDescription(pre + "baguette", key="bread_baguette"),

        ImageDescription(pre + "tv remote", key="tv_remote"),
        ImageDescription(pre + "Iphone", key="iphone"),
        ImageDescription(pre + "android smartphone", key="smartphone"),
        ImageDescription(pre + "nokia 3310", key="nokia_3310"),
        ImageDescription(pre + "the nintendo switch", key="nintendo_switch"),

        ImageDescription(pre + "salad", key="salad"),
        ImageDescription(pre + "tomato_salad", key="tomato_salad"),
        ImageDescription(pre + "bacon slice", key="bacon"),

        ImageDescription(pre + "ketchup", key="ketchup"),
        ImageDescription(pre + "aromat", key="aromat"),
        ImageDescription(pre + "mayonnaise tube", key="mayonnaise"),

        ImageDescription(pre + "air frier", key="air_frier"),
        ImageDescription(pre + "washing machine", key="washing_machine"),
        ImageDescription(pre + "smart tv", key="smart_tv"),
        ImageDescription(pre + "oven", key="oven"),
        ImageDescription(pre + "electric wall socket", key="electric_wall_socket"),
    ]
    return descriptions


class ImageGenerationApp:
    def __init__(self, base_folder: str) -> None:
        self.times = 12
        self.base_folder = base_folder
        factory = TextToImageFactory(1024, 1024)
        text_to_image: TextToImage = factory.hyper_sd()
        background_removal = BackgroundRemovalFactory().background_remover()
        self.generator = ImageGenerator(text_to_image=text_to_image,
                                        background_removal=background_removal)

    def generate(self, description: ImageDescription) -> None:
        images = self.generator.generate(prompt=description.prompt,
                                         times=self.times
                                         )
        index = 0
        for image in images:
            directory = os.path.join(self.base_folder, description.key)
            os.makedirs(directory, exist_ok=True)
            filename = os.path.join(directory, f"{description.key}_{str(index)}.png")
            image.save(filename, "PNG")
            index += 1


def main() -> None:
    app = ImageGenerationApp("../output")
    descriptions = generate_descriptions()
    for description in descriptions:
        app.generate(description)


if __name__ == '__main__':
    main()
