import {StyleSheet, View} from 'react-native';
import React from "react";
import Preview from "./components/Preview";
import Editor from "./components/Editor";
import Layout from "./components/Layout/Layout";

export default function App() {
    return (
        <View style={styles.container}>
            <Layout>
                <Editor text={'# Ciao\n\nHello World!'}/>
                <Preview/>
            </Layout>
            {/*<Preview/>*/}

            {/*<Text>Open up App.tsx to start working on your app!</Text>*/}
            {/*<StatusBar style="auto"/>*/}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
