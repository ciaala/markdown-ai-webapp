import {ReactElement, useState} from "react";
import SimpleMDE from "react-simplemde-editor";

export default function _Editor(): ReactElement {
    const [value, setValue] = useState('Hello, **world**!');

    const handleChange = (value: string) => {
        setValue(value);
    };

    return (
        <div>
            <SimpleMDE value={value} onChange={handleChange}/>
        </div>
    );
}