import {StyleSheet} from "react-native";
import {ReactElement} from "react";
const css = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#f00',
        alignItems: 'center',
        justifyContent: 'center',
    },

});
export default function Layout(props: {children: ReactElement[]}) : ReactElement  {
    return <div style={css.container}>{props.children}</div>;
}
