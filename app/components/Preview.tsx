import React, {ReactElement} from 'react'
import Markdown from 'react-native-markdown-display';

const markdown = '# Hi, *Pluto*!'

function Preview(): ReactElement {

    return <div>
        <Markdown>
            {markdown}
        </Markdown>
    </div>
}

export default Preview;