import React, {ReactElement} from 'react';
import MarkdownIt from 'markdown-it';
import MdEditor from 'react-markdown-editor-lite';
// import style manually
import 'react-markdown-editor-lite/lib/index.css';

const mdParser = new MarkdownIt(/* Markdown-it options */);

// Finish!

export default function Editor({text}:{text:string}): ReactElement {
    function handleEditorChange({html, text}: { html: string, text: string }) {
        console.log('handleEditorChange', html, text);
    }

    return (
        <MdEditor style={{height: '600px'}}
                  // config={{fullscreen:true}}
                  renderHTML={text => mdParser.render(text)}
                  onChange={handleEditorChange}
        value={text}/>
    );
};